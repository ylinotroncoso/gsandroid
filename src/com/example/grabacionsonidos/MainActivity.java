package com.example.grabacionsonidos;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;


public class MainActivity extends Activity implements OnCompletionListener {
    TextView tv1;
    Chronometer cronometro;
    MediaRecorder recorder;
    MediaPlayer player;
    File archivo;
    Button b1, b2, b3,b4,b5;
    //variable para acumular el tiempo actual al pausar la reproduccion
    long lengthCrono=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cronometro=(Chronometer) this.findViewById(R.id.cronometro);
        tv1 = (TextView) this.findViewById(R.id.textView1);
        b1 = (Button) findViewById(R.id.button1);
        b1.setBackgroundColor(Color.argb(100,90,90,90));
        b2 = (Button) findViewById(R.id.button2);
        b2.setBackgroundColor(Color.argb(100,90,90,90));
        b2.setEnabled(false);
        b3 = (Button) findViewById(R.id.button3);
        b3.setBackgroundColor(Color.argb(100,90,90,90));
        b3.setEnabled(false);
        b4 = (Button) findViewById(R.id.button4);
        b4.setBackgroundColor(Color.argb(100,90,90,90));
        b4.setEnabled(false);
        b5 = (Button) findViewById(R.id.button5);
        b5.setBackgroundColor(Color.argb(100,90,90,90));
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	   switch (item.getItemId()) {
    	   case R.id.cerrar:
    	      finish();
    	      return true;
    		}
		return false;
    }
    
    //metodo para grabar el sonido
    public void grabar(View v) {
        recorder = new MediaRecorder();
        cronometro.setBase(SystemClock.elapsedRealtime());
        cronometro.start();
        //formatos en los cuales se guarda el archivo de audio
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        //ruta en la que se guardara el archivo de sonido
        File path = new File(Environment.getExternalStorageDirectory()
        		.getAbsolutePath()
				+ "/Grabadora");
        path.mkdirs();
        //crear el archivo y nombrarlo con su extension
        try {
            archivo = File.createTempFile("grabacion", ".aac", path);
        } catch (IOException e) {
        }
        recorder.setOutputFile(archivo.getAbsolutePath());
        try {
            recorder.prepare();
        } catch (IOException e) {
        }
        //comenzar la grabacion del sonido
        recorder.start();
        tv1.setText("Grabando");
        b1.setEnabled(false);
        b2.setEnabled(true);
    }
    //detener la grabacion de audio y el cronometro
    public void detener(View v) {
        recorder.stop();
        cronometro.stop();
        player = new MediaPlayer();
        player.setOnCompletionListener(this);
        try {
            player.setDataSource(archivo.getAbsolutePath());
        } catch (IOException e) {
        }
        try {
            player.prepare();
        } catch (IOException e) {
        }
        b1.setEnabled(true);
        b2.setEnabled(false);
        b3.setEnabled(true);
        tv1.setText("Listo para reproducir");
    }
    //reproducir el archivo de sonido
    public void reproducir(View v) {
        player.start();
        //para iniciar el cronometro a 0 cuando se inicie la reproduccion o sino continuar en el tiempo actual hasta que finalice
        if(lengthCrono==0){
        	cronometro.setBase(SystemClock.elapsedRealtime());
            cronometro.start();
        }
        else{
        	cronometro.setBase(cronometro.getBase() + SystemClock.elapsedRealtime() - lengthCrono);
            cronometro.start();
        }
        b1.setEnabled(false);
        b2.setEnabled(false);
        b3.setEnabled(false);
        b4.setEnabled(true);
        tv1.setText("Reproduciendo");
    }
    //metodo para detener la reproduccion
    public void detenerRepro(View v){
    	player.pause();
    	//acumula el tiempo actual al parar la reproduccion
    	lengthCrono = SystemClock.elapsedRealtime();
    	cronometro.stop();
    	b1.setEnabled(true);
    	b2.setEnabled(false);
    	b3.setEnabled(true);
    	b4.setEnabled(false);
    }
 //metodo al finalizar la reproduccion del archivo
    public void onCompletion(MediaPlayer mp) {
        b1.setEnabled(true);
        b2.setEnabled(false);
        b3.setEnabled(true);
        b4.setEnabled(false);
        tv1.setText("Listo");
        //parar cronometro e inicializar la variable que acumula el tiempo
        lengthCrono=0;
        cronometro.stop();
    }
    
    public void listarArchivos(View v){
    	Intent i = new Intent(this, ListarArchivos.class);
    	startActivity(i);
    }
    
}

